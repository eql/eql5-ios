(in-package :cl-user)

(defpackage :my
  (:use :cl :eql :qml)
  (:export
   #:reload-qml
   #:start))
