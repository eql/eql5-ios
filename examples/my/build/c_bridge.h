#ifndef C_BRIDGE_H
#define C_BRIDGE_H

#include <QtCore>

QT_BEGIN_NAMESPACE

class CBridge : public QObject {
    Q_OBJECT
public:
    CBridge(QObject*, const QString&);

    Q_INVOKABLE void load(const QString&);
    Q_INVOKABLE void disableIdleTimer(bool);
};

QT_END_NAMESPACE

#endif
