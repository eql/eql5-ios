xcodebuild build \
  -project sensors.xcodeproj \
  -scheme sensors \
  -configuration Debug \
  -destination generic/platform=iOS \
  -destination-timeout 1 \
  ENABLE_ONLY_ACTIVE_RESOURCES=NO \
  ENABLE_BITCODE=NO \
  -allowProvisioningUpdates
