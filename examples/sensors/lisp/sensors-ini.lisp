(in-package :sensors)

(qrequire :quick)
(qrequire :network)

(defun start ()
  (eval:ini)
  (qml:ini-quick-view "qml/sensors.qml")
  (qconnect qml:*quick-view* "statusChanged(QQuickView::Status)" ; for reloading
            (lambda (status)
              (case status
                (#.|QQuickView.Ready|
                 (qml-reloaded))
                (#.|QQuickView.Error|
                 (qmsg (x:join (mapcar '|toString| (|errors| *quick-view*))
                               #.(make-string 2 :initial-element #\Newline)))))))
  (qlater 'ini)
  (qlater (lambda () (eval:eval-in-thread "(help)" nil)))) ; show help

;;; REPL

(defun show-repl (show) ; called from QML
  (when show
    (q> |opacity| ui:*repl-container* 0)
    (q> |visible| ui:*repl-container* t))
  (dotimes (n 10)
    (q> |opacity| ui:*repl-container* (/ (if show (1+ n) (- 9 n)) 10))
    (qsleep 0.015))
  (unless show
    (q> |visible| ui:*repl-container* nil))
  ;; sensor processing would interfere with REPL (on some devices)
  (if show
      (stop-sensor-timers)
      (start-sensor-timers)))

(defvar *reload-url* nil)

(defun ensure-url (url)
  (when url
    (setf *reload-url* url))
  (unless *reload-url*
    (format *query-io* "*** ensure 'web-server.sh' is running ***~
                      ~%WiFi IP (desktop computer): 192.168.1.")
    (let ((x (read *query-io*)))
      (when (and (integerp x)
                 (<= 1 x 255))
        (setf *reload-url* (format nil "http://192.168.1.~D:8080/" x))))))

;;; load remote Lisp file

(let ((ini t))
  (defun eql::load* (file &optional url)
    "Load Lisp file from an url."
    (ensure-url url)
    (when ini
      (setf ini nil)
      (load-lib "load-from-url"))
    (let ((remote-file (x:cc *reload-url* file)))
      (load-from-url remote-file)
      remote-file)))

(export 'eql::load* :eql)

;;; reload QML from remote

(defun reload-qml (&optional url)
  "Reload QML file(s) from an url."
  (ensure-url url)
  (let ((src (|toString| (|source| qml:*quick-view*))))
    (if (x:starts-with "qrc:/" src)
        (|setSource| qml:*quick-view* (qnew "QUrl(QString)"
                                            (x:string-substitute *reload-url* "qrc:/" src)))
        (qml:reload))
    (|toString| (|source| qml:*quick-view*))))

(defun qml-reloaded ()
  (ini))
