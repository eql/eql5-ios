
Info
----

This is a (simplified and extended) port of the Qt QML example **accelbubble**.

Please see example [../my](../my) for building/deploying the app or running it
on the desktop.


Note
----

Because of the moving bubble, this app has a fixed screen orientation (see
`ios/Info.plist`).

