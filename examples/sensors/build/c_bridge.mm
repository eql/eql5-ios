#include "c_bridge.h"
#include <ecl/ecl.h>

#import <UIKit/UIKit.h>

#undef SLOT

#define QSLOT(x)   "1"#x
#define QSIGNAL(x) "2"#x

extern "C" {
    void init_lib_ASDF(cl_object);
    // doesn't currently work in iOS
    //void init_lib_ECL_QUICKLISP(cl_object);
    void ini_load_from_url(cl_object);
    // uncomment if used
    //void ini_libs(cl_object);
}

QT_BEGIN_NAMESPACE

CBridge::CBridge(QObject* parent, const QString& name) : QObject(parent) {
    setObjectName(name); }

void CBridge::load(const QString& name) {
    if("asdf" == name) {
        ecl_init_module(NULL, init_lib_ASDF); }
    // doesn't currently work in iOS
    //else if("ecl-quicklisp" == name) {
    //    ecl_init_module(NULL, init_lib_ECL_QUICKLISP); }
    else if("load-from-url" == name) {
        ecl_init_module(NULL, ini_load_from_url); }
    // uncomment if used
    //else if("libs" == name) {
    //    ecl_init_module(NULL, ini_libs); }
}

void CBridge::disableIdleTimer(bool disable) {
    [UIApplication sharedApplication].idleTimerDisabled = (disable ? YES : NO); }

QT_END_NAMESPACE

