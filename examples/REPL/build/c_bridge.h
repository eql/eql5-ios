#ifndef C_BRIDGE_H
#define C_BRIDGE_H

#include <QApplication>
#include <QTouchEvent>
#include <QtDebug>

QT_BEGIN_NAMESPACE

class CBridge : public QObject {
    Q_OBJECT
public:
    CBridge(QObject*, const QString&);

    Q_INVOKABLE void load(const QString&);
    Q_INVOKABLE void disableIdleTimer(bool);
};

class EventApplication : public QApplication {
    Q_OBJECT
public:
    EventApplication(int& argc, char** argv) : QApplication(argc, argv) {
        installEventFilter(this); }

    bool eventFilter(QObject* object, QEvent* event) override {
        QEvent::Type type = event->type();
        if(type == QEvent::TouchBegin) {
            // workaround, see description in 'editor.lisp'
            if(strstr(object->metaObject()->className(), "Button") != nullptr) {
                buttonPressed(object->objectName()); }}
        else if(type == QEvent::InputMethod) {
            QString s = ((QInputMethodEvent*)event)->commitString();
            if(s.size() == 1) {
                bool changed = true;
                const int code = s.at(0).unicode();
                switch(code) {
                    // capture Tab (external keyboard), since QML Keys doesn't capture Tab
                    case '\t':
                        s = QString();
                        tabPressed(object->objectName());
                        break;
                    // replace iOS smart quotation marks with standard ones
                    // (English, Frensh, German, ...)
                    case 8216:
                    case 8217:
                    case 8218:
                        s[0] = QChar('\'');
                        break;
                    case 171:
                    case 187:
                    case 8220:
                    case 8221:
                    case 8222:
                        s[0] = QChar('"');
                        break;
                    // capture ALT+E, ALT+L key events (special key bindings)
                    case 8364:
                        s = QString();
                        altPressed('E'); // Expression: select expression
                        break;
                    case 172:
                        s = QString();
                        altPressed('L'); // Lambda: eval expression
                        break;
                    default:
                        changed = false; }
                if(changed) {
                    ((QInputMethodEvent*)event)->setCommitString(s); }}
            else if(s.size() == 2) {
                s = s.trimmed();
                bool changed = true;
                const int code = s.at(0).unicode();
                switch(code) {
                    // replace iOS smart quotation marks with standard ones
                    // (Frensh, ...)
                    case 171:
                    case 187:
                        s[0] = QChar('"');
                        break;
                    default:
                        changed = false; }
                if(changed) {
                    ((QInputMethodEvent*)event)->setCommitString(s); }}}
        return QApplication::eventFilter(object, event); }

Q_SIGNALS:
    void buttonPressed(const QString&);
    void tabPressed(const QString&);
    void altPressed(const QChar&);
};

QT_END_NAMESPACE

#endif
