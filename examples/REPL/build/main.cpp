#undef SLOT

#include <ecl/ecl.h>
#include <eql5/eql.h>
#include <QApplication>
#include <QTextCodec>
#include <QFileInfo>
#include <QTranslator>
#include "c_bridge.h"

extern "C" {
    void ini_app(cl_object);
    void init_lib_DEFLATE(cl_object);
    void init_lib_ECL_CDB(cl_object);
    void init_lib_ECL_HELP(cl_object);
    void init_lib_QL_MINITAR(cl_object);
    void init_lib_SOCKETS(cl_object);
    void init_lib_ECL_CURL(cl_object);
}

QT_BEGIN_NAMESPACE

int main(int argc, char** argv) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    EventApplication qapp(argc, argv);
    //qapp.setOrganizationName("MyTeam");
    //qapp.setOrganizationDomain("my-team.org");
    qapp.setApplicationName(QFileInfo(qapp.applicationFilePath()).baseName());

    QTextCodec* utf8 = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(utf8);

    // your own translation files
    // (for including translations used by Qt itself, please see Qt assistant)
    QTranslator tr;
    if(tr.load("my_" + QLocale::system().name().left(2), ":/")) {
        qapp.installTranslator(&tr); }

    // this was needed in past for GC on iOS
    /*
    const int size = 256;
    char* buffer[size];
    buffer[0] = "";
    GC_allow_register_threads();
    GC_register_my_thread((const struct GC_stack_base*)buffer);
    GC_stackbottom = (char*)(buffer + size - 1);
    setenv("ECLDIR", "", 1);
    */

    EQL eql;
    CBridge c_bridge(&qapp, "c_bridge");
    ecl_init_module(NULL, ini_app);

    // load contrib libs in binary version (byte compiled FASC files are slow)
    ecl_init_module(NULL, init_lib_DEFLATE);
    ecl_init_module(NULL, init_lib_ECL_CDB);
    ecl_init_module(NULL, init_lib_ECL_HELP);
    ecl_init_module(NULL, init_lib_QL_MINITAR);
    ecl_init_module(NULL, init_lib_SOCKETS);
    ecl_init_module(NULL, init_lib_ECL_CURL);

    EQL::eval("(in-package :eql-user)");
    // fallback restart for connections from Slime
    EQL::eval("(loop (with-simple-restart (restart-qt-events \"Restart Qt event processing.\") (qexec)))");

    return 0; }

QT_END_NAMESPACE

