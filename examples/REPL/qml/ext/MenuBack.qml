import QtQuick 2.10
import QtQuick.Controls 2.10
import "." as Ext

Rectangle {
    id: menuBack
    width: main.width
    height: backButton.height
    color: "#f0f0f0"

    property string label

    signal pressed()

    Button {
        id: backButton
        height: main.isPhone ? 40 : 46

        background: Rectangle {
            Text {
                id: iconBack
                x: 10
                height: backButton.height
                verticalAlignment: Text.AlignVCenter
                font.family: fontAwesome.name
                font.pixelSize: 34
                color: "#007aff"
                text: "\uf104"
            }
            Text {
                x: 30
                height: backButton.height * 1.1 // align correction (different font from above)
                verticalAlignment: Text.AlignVCenter
                font.weight: Font.DemiBold
                color: iconBack.color
                text: "Repl"
            }
            implicitWidth: 90
            color: menuBack.color
        }

        onPressed: parent.pressed()
    }

    Text {
        anchors.centerIn: parent
        text: menuBack.label
        font.weight: Font.DemiBold
    }
}
