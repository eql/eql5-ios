(load "lisp/3rd-party/sokoban")
(load "lisp/3rd-party/my-levels")
(load "lisp/ui-vars")
(load "lisp/sokoban")

(progn
  (qsoko:start)
  (|showMaximized| qml:*quick-view*))
