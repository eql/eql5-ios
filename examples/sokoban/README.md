
### Prepare

You need to prepare your iOS development environment first, as described in
[README-PREPARE](../../README-PREPARE.md).



### Prerequisits

For compiling, deploying and debugging an app you will need neither Xcode nor
Qt Creator: the command line will do.

As you probably know, you'll also need an Apple ID to be able to build and
deploy iOS apps. The web is your friend (and no, you don't need to be a
registered developer, this is only needed for the AppStore).


### Make (cross-compile)

For every new project, and every time you make changes to your `sokoban.pro`
file, run this script for creating/updating an Xcode project:

```
  ./make-xcodeproject.sh
```
The following 2 scripts will cross-compile the Lisp and Qt5 files and create an
app bundle, ready to be deployed to the device (no IPA file needed):

```
  ./ecl-ios.sh
  ./xcodebuild.sh
```

If the build succeeded, you can deploy the app directly to your device: it will
use **wifi** if not connected via **usb**:

```
  ./deploy-app.sh
```

N.B: Always run script `clean-lisp-files.sh` after changing your ECL version!


### Debugging

Since `ios-deploy` also offers command line debugging, you may use these
scripts to build a debug version, deploy and launch it from the debugger:

```
  ./debug-xcodebuild.sh
  ./debug-deploy.sh

  ./debug-app.sh
```
This is (of course) only needed if your ECL crashes your app; for debugging
the app itself, just run it on the desktop, see below.


### Developing your app

To just run this example on the desktop (requires ECL and EQL5 desktop versions
to be installed), do:

```
  $ eql5 run.lisp
```

For interactive development of both Lisp and QML, you should use Slime (see
docu in EQL5 desktop how to start Swank and connect to it).

So, to run this example in Slime, do:

```
  $ cd EQL5-iOS/examples/sokoban
  $ eql5 ~/slime/eql5-start-swank run.lisp
```

Now, in Emacs connect to the above Swank, and hack both Lisp (as usual) and QML
using `(qsoko:reload-qml)` after changes made to QML.


### Notes

Please see comment in `sokoban.pro` about `Info.plist`.
